# Troubadour 
### (Slovene version of the name: Trubadur)

Gamified music theory e-learning mobile platform. Currently includes melodic and rhythmic exercises in single and multiplayer modes. The repository currently includes two applications for melodic intervals and rhythmic dictation exercises.

The platform offers automatic generation of exercises, which can be further adjusted by individual teachers. It is a web-based platform with a responsive interface that adjusts its layout to different mobile devices. Based on the individual students' exam results gathered in this research, the exercises can be further adjusted specifically to improve the individual student's performance. Focused on the music theory learning, the Troubadour platform offers an intuitive visual representation of the music score. With gamification features, including badges, leaderboards and a multiplayer mode---the Troubadour platform aims to engage the students in exercising and boosting their knowledge. The platform therefore represents a  student-engaging learning environment for music theory, supplementing existing learning management systems, such as Moodle.

Production version: [https://trubadur.si](https://trubadur.si) (Slovene language)

The melodic intervals application was implemented as a part of a master's thesis (Žiga Vučko). The rhythmic dictation application was implemented as a part of abachelor's thesis (Lovro Suhadolnik).

Full text: https://repozitorij.uni-lj.si/Dokument.php?id=116181&lang=slv (Slovene language)

The repository is maintained by the members of the Laboratory for computer graphics and multimedia, Faculty of computer and information science, University of Ljubljana.

## Showcase screenshots
![Platform menu](screenshots/dashboard.png)
![User profile](screenshots/profile.png)

# Initial setup

## Requirements

1. Linux server
2. Nginx / Apache
3. MySQL / MariaDB
 
## Production Deployment

1. Git clone https://bitbucket.org/ul-fri-lgm/troubadour_production
2. composer install
3. npm install
4. npm run production
5. Copy .env.example -> .env
6. Create database `trubadur` in mysql/mariadb (Or your chosen name, if you set a different one in .env)
7. Change database connection info in .env
8. run `artisan key:generate` and copy the string that starts with base64: and ends with =
9. Paste the string into .env into `APP_KEY` key.. Like this: `APP_KEY=36aab74....`
10. `apt install ffmpeg fluidsynth`
11. `mkdir storage/midi`
12. `ln -s <absolute path to your folder>/storage/midi <absolute path to your folder>/public/audio`
13.  `chmod -R 777 storage`
14. Create folder 

Warning: Change `<absolute path to your folder>` in Step 12 depending on your install folder location.

# Melodic intervals application

No additional configuration is needed. Adjusting the individual interval distributions and other interval exercise parameters is available through the admin panel.
At this point, you should be able to play the intervals application. 


# Rhythmic dictation application

## Application setup:

Rhythm application currently has a different setup for adjusting individual difficulty levels and distributions. 
The code currently does not generate exercises and exercise sounds on demand, but uses an offline cache (more about the topic in the diploma thesis), which has to be built manually by using the following procedure. 
The procedure creates a fixed set of exercises, which are rndomly chosen by the API when user requests an exercise.

If the cache does not exist, rhythm game interface will freeze and won't work.

1. Import pre-made rhythm generating definitions (or create your own) into the database

```bash
mysql -u<username> -p<password> -t <databaseName> < rhythmGeneratingDefinitions.sql
```

2. For each level (Currently levels `11,12,13,14,21,22,23,24,31,32,33,34` are available) run:

```bash 
php artisan generate:rhythm <level> <number of different exercises>
```

The second parameter tells the program, how many exercises it should generate. If you generate a small number of them, the game will feel like it repeats itself a lot, because the engine won't have much exercises to choose from. Generation takes time depending on your server capabilities. It generates sound files and writes exercises to the database. 

Warning: Due to some incomplete rhythm definitions, the program crashes sometimes while generating cache for some levels. You can safely repeat the procedure over and over again until you have sufficient number of exercises created.

To clear the cache and start over, you can use:

```bash
php artisan generate:rhythm:clear <level>
```

This deletes all sound files. There is a restriction, that the engine cannot use the exercise, if it doesn't have its soud file. Therefore, old exercises will remain in the database (to keep relationships between games and leaderboard scores), but they won't be available for use until you recreate the sound file again.

## Creating new rhythm definitions

You can create your own rhythm definitions from your own music, if you can export it to MusicXML.

The procedure is based on three steps:

- Convert MusicXML to Musison (our proprietary format). 
    - You can write definitions for multiple rhythm bars in the same musicXML file
    - After this step, there will be one file for each rhythm level in the provided MusicXML file
- Extract distinct rhythm features from generated filed and calculate their probability
    - After this step, you will have one json file with feature probabilities for each rhythm level
    - Extracted rhythm bars will be written to the database automatically.
    - When the program starts, it reads all existing rhythm bars. Throughout the process, the program will compare each found bar with all existing bars, so there will be no duplicates in the database.
- Write rhythm definitions to database, so they can be used in exercise generation engine
    - After this step, your music bars will be ready to be used in rhythm exercises.

1. Write some music in your favourite Music Composition program (Sibelius, FInale are all fine)
2. Export your music into MusicXML file
3. Run

```bash
php artisan import:SplitFileByBars <musicXML file path> <output folder for musison files> <bar_defs>
```

bar_defs is a string, of format

```text
<num_beats1>|<rhythm_level1>,<num_beats2>|<rhythm_level2>,<num_beats3>|<rhythm_level4>,...
```

The program assumes, that the MusicXML consists of music bars for different levels. 

For example: We created a musicXML file named `textXML.xml` which has 32 music bars. The first 16 are for rhythm level 11, and the second 16 are for rhythm level 12. We want the output Musison to be stored in folder called `SplitBars`. In this situation, we would call the program like so:

```bash
php artisan testXML.xml SplitBars 16|11,16|12
```

The program will produce the following files:

```text
./SplitBars/11_16.json
./SplitBars/12_16.json
```



2. For each output file, run

```bash
php artisan import:ExtractRhythmFeatures <file> <output file>
```

The output file is a file, not a folder. This command will produce one file per run. To continue our example: We want the extracted features to take place in a folder called `ExtractedFeatures` We would run the script like so:

```bash
php artisan import:ExtractRhythmFeatures SplitBars/11_16.json ExtractedFeatures/11_16.json
php artisan import:ExtractRhythmFeatures SplitBars/12_16.json ExtractedFeatures/12_16.json
```

The program will produce:

```text
./ExtractedFeatures/11_16.json
./ExtractedFeatures/12_16.json
```

3. For each generated file in previous step, run

```bash
php artisan import:WriteRhythmFeatures <file> <rhythm_feature_name> <rhythm_level>
```

First parameter is the input file.

The second parameter is the name of the newly created rhythm feature. You can read more about those in the thesis.

The last parameter is the rhythm level the file represents.

Continued example: We want to create rhythm features called Test123_11 and Test123_12. We would run:

```bash
php artisan import:WriteRhythmFeatures ExtractedFeatures/11_16.json Test123_11 11
php artisan import:WriteRhythmFeatures ExtractedFeatures/12_16.json Test123_12 12
```



## Export all rhythm definitions in the database

Run:

```bash
./exportRhythmDefinitions.sh
```

This will produce a file named `rhythmGeneratingDefinitions.sql`,  which you can use to import your definitions to another instance. We recommend commiting your directory before using this command, since it overwrites the existing file with the same name.

# Contact

If you experience any issues or wish to contribute to the platform, feel free to contact us at lgm@lgm.fri.uni-lj.si